var express = require('express');
var router = express.Router();
var config = require('../config/envConfig.js');
const line = require('@line/bot-sdk');
const AssistantController = require('../controller/AssistantController');
const MongodbContro = require('../controller/MongodbController');
const logger = require('../services/AssistantLogger');

const client = new line.Client({
  channelAccessToken: config.channelAccessToken
});

MongodbContro.init();

/* GET home page. */
router.post('/line', async function (req, res, next) {

  // console input data
  logger.log(JSON.stringify(req.body, null, 2));
  res.status(200).send();

  let targetEvent = req.body.events[0];

  if (targetEvent.type == 'message') {

    if (targetEvent.message.type == 'text') {

      let userSay = targetEvent.message.text;
      let userId = targetEvent.source.userId;

      logger.log('user say : ', userSay);
      logger.log('user id : ', userId);

      let AssistantReturn = await AssistantController.sendMessage(userId, userSay);
      console.log(AssistantReturn);
      let returnMessage = {
        type: 'text',
        text: AssistantReturn.output.generic[0].text
      };

      replyToLine(targetEvent.replyToken, returnMessage);
      logger.log(userId, 'return message:', returnMessage);

      let document = {
        timestamp: Date.now(),
        userId: userId,
        userSay: userSay,
        AssistantReturn: AssistantReturn
      }
      MongodbContro.insertOne('LineBot', 'chatBot', document);
    }
  }
});

module.exports = router;

function replyToLine(replyToken, messages) {
  client.replyMessage(replyToken, messages)
    .then(() => {
      console.log("Success !!");
    })
    .catch((err) => {
      console.log(err);
    });
}

logger.init('webhook');