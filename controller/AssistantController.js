(function () {

    const config = require('../config/envConfig.js');

    const assistantAPI = require('../services/AssistantAPIV2');
    assistantAPI.init(config.assistant)

    let userSessions = [];

    class AssistantController {
        constructor() {
            //this.sendMessage = this.sendMessage.bind(this);
        }

        // 取得 user session 
        async sendMessage(userId, text) {

            // 取出API所需暫存Session
            let userSession = userSessions[userId];

            // 若沒有對應session
            if (!userSession) {
                // create new session
                let newSession = await assistantAPI.createSession();
                userSession = newSession.session_id;
                userSessions[userId] = userSession;
            }

            // 以下進行QA
            let assistantAns = await assistantAPI.message(text, userSessions[userId]);
            console.log(JSON.stringify(assistantAns, null, 2));

            return assistantAns;
        }
    }
    module.exports = new AssistantController();
}());