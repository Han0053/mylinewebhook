const config = require('../config/envConfig');
const MongoClient = require('mongodb').MongoClient;
const url = config.mongodb;

class MongodbController {
    constructor() {
        this.init = this.init.bind(this);
        this.insertOne = this.insertOne.bind(this);
    }
    init() {
        let _this = this;
        return new Promise(resolve => {
            MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
                if (err) {
                    console.log('connect error: ', err);
                } else {
                    console.log('connect success!!');
                    _this.db = db;
                }
            })
            resolve(err || 'success');
            // Solve UnhandledPromiseRejectionWarning
        }).catch(new Function())
    }
    insertOne(dbName, collection, document) {
        return new Promise(resolve => {
            let dbo = this.db.db(dbName);
            dbo.collection(collection).insertOne(document, function (err, res) {
                if (err) {
                    console.log('connect error: ', err);
                } else {
                    console.log('connect success!!');
                }
                resolve(err || 'success');
            });
        });
    };
}
module.exports = new MongodbController();